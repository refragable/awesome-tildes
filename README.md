# Depracated

The official awesome-tildes project is depracated and all effort is now being directed toward the [Unofficial Tildes Wiki](https://unofficial-tildes-wiki.gitlab.io/)

# Awesome Tildes

> A list of client-side extensions, CSS themes, and scripts developed and maintained by and for [tildes.net](https://tildes.net) users.

## Adding to This List

1. Fork the repository.
2. Add your entry with relevant information in the correct section.
3. Push the changes.
4. Submit a merge request.

OR 

1. Create a new issue with the entry template.
2. Fill in the information.
3. Submit the issue.

## Extensions

| Name | Author | Description |
| ---- | ------ | ----------- |
| [Extended](https://github.com/theCrius/tildes-extended) | [crius](https://tildes.net/user/crius) | Extension for Chrome and Firefox to enhance the UX/UI. |
| [Plus](https://github.com/avinassh/tildes-plus) | [tcp](https://tildes.net/user/tcp) | Extension to add markdown previews to posts and comments. |

## Scripts

| Name | Author | Description |
| ---- | ------ | ----------- |
| [Tildenhancer](https://greasyfork.org/en/scripts/369273-tildenhancer) | [thykka](https://tildes.net/user/thykka) | Various UI/UX enhancements, including automatic night mode. | 

## CSS Themes

| Name | Author | Description |
| ---- | ------ | ----------- |
| [Compact](https://gitlab.com/Bauke/styles/tree/master/css/tildes-compact) | [Bauke](https://tildes.net/user/Bauke) | Removes some elements and changes some sizes to make the layout a little more compact. |
| [Dracula](https://gitlab.com/Bauke/styles/tree/master/css/tildes-dracula) | [Bauke](https://tildes.net/user/Bauke) | Dracula theme. |
| [Monokai](https://gitlab.com/Bauke/styles/tree/master/css/tildes-monokai) | [Bauke](https://tildes.net/user/Bauke) | Monokai theme. |
| [Dark & Flat](https://gitlab.com/jfecher/Tildes-DarkAndFlat) | [rndmprsn](https://tildes.net/user/rndmprsn) | A dark and flat theme + alt. version combined with [pfg](https://tildes.net/user/pfg)'s [Collapse Comment Bar](https://gist.github.com/pfgithub/1da7d2024a9748085fccfceaf2ce126d) |
| [Aqua Dark](https://openusercss.org/theme/5ba4643ffd8bc10c006d8c6c) | [Exalt (openusercss)](https://openusercss.org/profile/5b9f139c1a64280c0030ee4d) | A green/blue dark theme |
